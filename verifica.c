#include <stdio.h>
#include <stdlib.h>
#include "main.h"

int main(int argc, char const* argv[]) {
  if (argc < 2) {
    printf("Uso: %s <numero para verificar a ordem>\n", argv[0]);
    return 1;
  }

  int len = atoi(argv[1]);
  int* arr = malloc(sizeof(int) * len);

  for (int i = 0; i < len; i++) {
    scanf("%d", &arr[i]);
  }

  int err = 0;

  for (int i = 0; i < len - 1; i++) {
    if (arr[i] > arr[i + 1]) {
      printf("Item %d fora de ordem com %d (%d)\n", arr[i], arr[i + 1], i);
      err = 1;
    }
  }

  if (!err) {
    printf("Itens ordenados corretamente\n");
  } else {
    printf("Itens mal ordenados\n");
  }

  return err;
}
