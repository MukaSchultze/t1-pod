#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct bucket Bucket;

int main(int argc, char const* argv[]) {
  if (argc < 4) {
    printf("Uso: %s <numero de elementos a ordenar> <I/Q/S> <buckets>\n",
           argv[0]);
    return 1;
  }

  BUCKETS = atoi(argv[3]);

  int len = atoi(argv[1]);
  int* arr = malloc(sizeof(int) * len);

  for (int i = 0; i < len; i++) {
    scanf("%d", &arr[i]);
  }

  switch (argv[2][0]) {
    case 'I':
      bucketSort(arr, len, insertionSort);
      break;
    case 'Q':
      bucketSort(arr, len, quickSort);
      break;
    case 'S':
      bucketSort(arr, len, shellSort);
      break;
    default:
      printf("Algoritmo de ordenação '%c' inválido", argv[2][0]);
      break;
  }

  return 0;
}

void bucketSort(int* arr, int len, void (*sortingMethod)(int*, int)) {
  if (len <= 1) return;

  Bucket* buckets = createBucketArray(BUCKETS);

  int maior = arr[0];
  int menor = arr[0];

  for (int i = 1; i < len; i++) {
    if (arr[i] > maior) maior = arr[i];
    if (arr[i] < menor) menor = arr[i];
  }

  for (int i = 0; i < len; i++) {
    int bucketIndex = ((float)(arr[i] - menor) / (maior - menor)) * BUCKETS;

    if (bucketIndex >= BUCKETS) bucketIndex = BUCKETS - 1;

    pushToBucket(&buckets[bucketIndex], arr[i]);
  }

  for (int i = 0, b = 0; b < BUCKETS; b++) {
    sortingMethod(buckets[b].values, buckets[b].count);

    for (int j = 0; j < buckets[b].count; j++) {
      arr[i++] = buckets[b].values[j];
      printf("%d\n", arr[i - 1]);
    }

    free(buckets[b].values);
  }

  free(buckets);
}

Bucket createBucket() {
  Bucket r;
  r.count = 0;
  r.capacity = BUCKET_START_CAPACITY;
  r.values = malloc(sizeof(int) * r.capacity);
  return r;
}

Bucket* createBucketArray(int count) {
  Bucket* array = malloc(sizeof(Bucket) * count);

  for (int i = 0; i < count; i++) {
    array[i] = createBucket();
  }

  return array;
}

void pushToBucket(Bucket* bucket, int value) {
  if (bucket->count >= bucket->capacity) {
    expandBucket(bucket);
  }

  bucket->values[bucket->count] = value;
  bucket->count++;
}

void expandBucket(Bucket* bucket) {
  bucket->capacity *= BUCKET_CAPACITY_FACTOR;

  int* oldArray = bucket->values;
  int* newArray = malloc(sizeof(int) * bucket->capacity);

  bucket->values = newArray;

  memcpy(newArray, oldArray, bucket->count * sizeof(int));
  free(oldArray);
}

int compareIntegers(const void* a, const void* b) {
  int x = *((int*)a);
  int y = *((int*)b);

  return x == y ? 0 : x < y ? -1 : 1;
}

void insertionSort(int arr[], int length) {
  for (int i = 1; i < length; i++) {
    int key = arr[i];
    int j = i - 1;

    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      j = j - 1;
    }

    arr[j + 1] = key;
  }
}

void quickSort(int* arr, int length) {
  qsort(arr, length, sizeof(int), &compareIntegers);
}

void shellSort(int* arr, int length) {
  for (int gap = length / 2; gap > 0; gap /= 2) {
    for (int i = gap; i < length; i += 1) {
      int temp = arr[i];
      int j;

      for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
        arr[j] = arr[j - gap];

      arr[j] = temp;
    }
  }
}