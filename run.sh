gcc geraN.c -o geraN.out
gcc parcOrdAsc.c -o parcOrdAsc.out
gcc verifica.c -o verifica.out -std=c99
clear

set -e

echo Compilando...
echo 
gcc main.c -Wall -O3 -ansi -o main.out -std=c99
echo 

run_if_not_exists () {
    TO="numbers/$1.$2.numbers"
    FROM="numbers/$3.$2.numbers"

    if [ -e $TO ]
    then 
        echo $TO já existe
    else
        if [ -e $FROM ]
        then 
            /usr/bin/time --format "%C\tReal %E\tUser %U\tSystem %S" --output times.txt --append ./$1.out $2 < $FROM > $TO
        else
            /usr/bin/time --format "%C\tReal %E\tUser %U\tSystem %S" --output times.txt --append ./$1.out $2 > $TO
        fi 
    fi
}

run () {
    TO="numbers/$1.$2.numbers"
    FROM="numbers/$3.$2.numbers"

    if [ -e $FROM ]
    then 
        /usr/bin/time --format "%C\tReal %E\tUser %U\tSystem %S" --output times.txt --append ./$1.out $2 $4 $5 < $FROM > $TO
    else
        /usr/bin/time --format "%C\tReal %E\tUser %U\tSystem %S" --output times.txt --append ./$1.out $2 $4 $5 > $TO
    fi
}

run_all (){
  
    echo Gerando $1 números...
    run_if_not_exists geraN $1
    echo --------------------------------------
   
    echo Parcialmente ordenando $1 números...
    run_if_not_exists parcOrdAsc $1 geraN
    echo --------------------------------------

    echo Executando bucket sort com $1 números e modo $2 com $3 buckets...
    run main $1 geraN $2 $3
    echo --------------------------------------

    echo Verificando ordenação...
    run verifica $1 main
    echo --------------------------------------

    echo Executando bucket sort com $1 números parcialmente ordenados e modo $2 com $3 buckets...
    run main $1 parcOrdAsc $2 $3
    echo --------------------------------------
    
    echo Verificando ordenação...
    run verifica $1 main
    echo --------------------------------------

}

echo > times.txt
rm times.txt;
# rm result.txt;

run_all 100000 I 10000
run_all 100000 Q 10000
run_all 100000 S 10000
run_all 1000000 I 10000
run_all 1000000 Q 10000
run_all 1000000 S 10000
run_all 100000000 I 10000
run_all 100000000 Q 10000
run_all 100000000 S 10000

run_all 100000 I 1000
run_all 100000 Q 1000
run_all 100000 S 1000
run_all 1000000 I 1000
run_all 1000000 Q 1000
run_all 1000000 S 1000
run_all 100000000 I 1000
run_all 100000000 Q 1000
run_all 100000000 S 1000
 
run_all 100000 I 100
run_all 100000 Q 100
run_all 100000 S 100
run_all 1000000 I 100
run_all 1000000 Q 100
run_all 1000000 S 100
run_all 100000000 I 100
run_all 100000000 Q 100
run_all 100000000 S 100

echo Fim