
int BUCKETS;
int BUCKET_START_CAPACITY = 1;
int BUCKET_CAPACITY_FACTOR = 2;

typedef struct bucket {
  int count;
  int capacity;
  int* values;
} Bucket;

int main(int argc, char const* argv[]);
void bucketSort(int* arr, int len, void (*sortingMethod)(int*, int));

Bucket createBucket();
Bucket* createBucketArray(int count);

void pushToBucket(Bucket* bucket, int value);
void expandBucket(Bucket* bucket);

int compareIntegers(const void* a, const void* b);

void insertionSort(int arr[], int length);
void quickSort(int* vet, int length);
void shellSort(int* vet, int length);